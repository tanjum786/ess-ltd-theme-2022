function isFormIdPresentInURL() {
  let formId = "#hs_cos_wrapper_dnd_area-module-1_form";
  return window.location.hash === formId;
}
function toggleFormPopUp() {
  let formPopUp = document.querySelector('.form-popup');
  if (isFormIdPresentInURL()) {
closeButton.classList.remove('overlay-active');
//     formPopUp.classList.remove('fm-act-not');
    formPopUp.classList.add('form-active');
  } else {
    formPopUp.classList.add('fm-act-not');
  }
}
window.addEventListener('DOMContentLoaded', function() {
  toggleFormPopUp();
});

let closeButton = document.querySelector('.overlay_form_close');
closeButton.addEventListener('click', function() {
  let formPopUp = document.querySelector('.form-popup');
//   formPopUp.classList.add('fm-act-not');
  formPopUp.classList.remove('form-active');
  closeButton.classList.remove('overlay-active');
});
