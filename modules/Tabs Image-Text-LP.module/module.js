
    document.addEventListener('DOMContentLoaded', function () {
        var tabButtons = document.querySelectorAll('.tabs_button .tab-button');
        var tabContents = document.querySelectorAll('.tabs-contents .tab-content');
    
        // Make the first tab active by default
        if (tabButtons.length > 0 && tabContents.length > 0) {
            tabButtons[0].classList.add('active');
            tabContents[0].classList.add('active');
        }
    
        tabButtons.forEach(function (button, index) {
            button.addEventListener('click', function () {
                // Remove 'active' class from all buttons and content
                tabButtons.forEach(function (btn) {
                    btn.classList.remove('active');
                });
                tabContents.forEach(function (content) {
                    content.classList.remove('active');
                });
    
                // Add 'active' class to the clicked button and content
                this.classList.add('active');
                tabContents[index].classList.add('active');
            });
        });
    });
    
    
