 document.addEventListener('DOMContentLoaded', function () {
    let btnPopQuote = document.querySelector('.btn-pop-quote ');

    btnPopQuote.addEventListener('click', function (event) {
      event.preventDefault(); 
      let formPopup = document.querySelector('.form-popup');
      formPopup.classList.remove('fm-act-not');

      formPopup.classList.add('form-active');
    });
  });