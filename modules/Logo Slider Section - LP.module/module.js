$('.slide-lp').slick({
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 2,
    arrows: true,
    dots: false,
     autoplay: true,
    autoplaySpeed: 1000,
     responsive: [
       {
         breakpoint: 1221,
         settings: {
           slidesToShow: 4, 
           slidesToScroll: 2
         }
       },
       {
         breakpoint: 768,
         settings: {
           slidesToShow: 3, 
           slidesToScroll: 1
         }
       },
       {
         breakpoint: 480,
         settings: {
           slidesToShow: 1,
          
         }
       }
       // You can unslick at a given breakpoint now by adding:
       // settings: "unslick"
       // instead of a settings object
     ]
  });

