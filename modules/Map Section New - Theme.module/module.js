var svg = document.querySelector("#Layer_1");
var pins = document.querySelectorAll("[data-value]");
var prevSvgElement = null;
for (var i = 0; i < pins.length; i++) {
  pins[i].addEventListener("click", function() {
    var value = this.getAttribute("data-value"); 
    var svgElement = document.querySelector("[data-value='" + value + "']");
    svgElement.style.fill = "#ea9523";
    if (prevSvgElement !== null) {
      prevSvgElement.style.fill = "";
    }
    prevSvgElement = svgElement;
  });
}




