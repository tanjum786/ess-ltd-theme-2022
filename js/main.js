(function () {
  // Polyfill for NodeList.prototype.forEach() in IE
  if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;
      for (var i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  }

  // Variables
  var nav = document.querySelector('.header__navigation');
  var langSwitcher = document.querySelector('.header__language-switcher');
  var search = document.querySelector('.header__search');
  var allToggles = document.querySelectorAll('.header--toggle');
  var navToggle = document.querySelector('.header__navigation--toggle');
  var langToggle = document.querySelector('.header__language-switcher--toggle');
  var searchToggle = document.querySelector('.header__search--toggle');
  var closeToggle = document.querySelector('.header__close--toggle');
  var allElements = document.querySelectorAll(
    '.header--element, .header--toggle'
  );
  var emailGlobalUnsub = document.querySelector('input[name="globalunsub"]');

  // Functions

  // Function for executing code on document ready
  function domReady(callback) {
    if (['interactive', 'complete'].indexOf(document.readyState) >= 0) {
      callback();
    } else {
      document.addEventListener('DOMContentLoaded', callback);
    }
  }

  // Function for toggling mobile navigation
  function toggleNav() {
    allToggles.forEach(function (toggle) {
      toggle.classList.toggle('hide');
    });

    nav.classList.toggle('open');
    navToggle.classList.toggle('open');

    closeToggle.classList.toggle('show');
  }

  // Function for toggling mobile language selector
  function toggleLang() {
    allToggles.forEach(function (toggle) {
      toggle.classList.toggle('hide');
    });

    langSwitcher.classList.toggle('open');
    langToggle.classList.toggle('open');

    closeToggle.classList.toggle('show');
  }

  // Function for toggling mobile search field
  function toggleSearch() {
    allToggles.forEach(function (toggle) {
      toggle.classList.toggle('hide');
    });

    search.classList.toggle('open');
    searchToggle.classList.toggle('open');

    closeToggle.classList.toggle('show');
  }

  // Function for the header close option on mobile
  function closeAll() {
    allElements.forEach(function (element) {
      element.classList.remove('hide', 'open');
    });

    closeToggle.classList.remove('show');
  }

  // Function to disable the other checkbox inputs on the email subscription system page template
  function toggleDisabled() {
    var emailSubItem = document.querySelectorAll('#email-prefs-form .item');

    emailSubItem.forEach(function (item) {
      var emailSubItemInput = item.querySelector('input');

      if (emailGlobalUnsub.checked) {
        item.classList.add('disabled');
        emailSubItemInput.setAttribute('disabled', 'disabled');
        emailSubItemInput.checked = false;
      } else {
        item.classList.remove('disabled');
        emailSubItemInput.removeAttribute('disabled');
      }
    });
  }

  // Execute JavaScript on document ready
  domReady(function () {
    if (!document.body) {
      return;
    } else {
      // Function dependent on language switcher
      if (langSwitcher) {
        langToggle.addEventListener('click', toggleLang);
      }

      // Function dependent on navigation
      if (navToggle) {
        navToggle.addEventListener('click', toggleNav);
      }

      // Function dependent on search field
      if (searchToggle) {
        searchToggle.addEventListener('click', toggleSearch);
      }

      // Function dependent on close toggle
      if (closeToggle) {
        closeToggle.addEventListener('click', closeAll);
      }

      // Function dependent on email unsubscribe from all input
      if (emailGlobalUnsub) {
        emailGlobalUnsub.addEventListener('change', toggleDisabled);
      }
    }
  });
})();

$(document).ready(function(){
 
    /** 
       * Mobile Nav
       *
       * Hubspot Standard Toggle Menu
       */
   
    $('.menu-popup .flyouts .hs-item-has-children > a').after(' <div class="child-trigger"><i></i></div>');
  
    $('.menu-trigger').click(function() {
       $('body').toggleClass('menu-show');
      $('.child-trigger').removeClass('child-open');
      $('.hs-menu-children-wrapper').slideUp(250);
      return false;
    });
   $('header.main-header-province:after').click(function() {
       $('body').toggleClass('menu-show');
      $('.child-trigger').removeClass('child-open');
      $('.hs-menu-children-wrapper').slideUp(250);
      return false;
    });
    
  //   close menu click outside the hamburger //
  $(document).on('click', function(event) {
    const menuContainer = $('.header-menu');
    const menuTrigger = $('.menu-trigger');
    if (!menuContainer.is(event.target) && !menuTrigger.is(event.target) && menuContainer.has(event.target).length === 0) {
      $('body').removeClass('menu-show');
    }
  });
  // end  //

    $('.child-trigger').click(function() {
      $(this).parent().siblings('.hs-item-has-children').find('.child-trigger').removeClass('child-open');
      $(this).parent().siblings('.hs-item-has-children').find('.hs-menu-children-wrapper').slideUp(250);
      $(this).next('.hs-menu-children-wrapper').slideToggle(250);
      $(this).next('.hs-menu-children-wrapper').children('.hs-item-has-children').find('.hs-menu-children-wrapper').slideUp(250);
      $(this).next('.hs-menu-children-wrapper').children('.hs-item-has-children').find('.child-trigger').removeClass('child-open');
      $(this).toggleClass('child-open');
      return false;
    });
   
  
  $('.slider-container').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    dots: true
  });
  
  $('.parts-image-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true
  });
 
  $('#ts-slide-form').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    adaptiveHeight: true
  });
  
  
  $('.ts-slide-bg').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    adaptiveHeight: true
  });

  $('.o2 .adv-outer').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 1221,
        settings: {
          slidesToShow: 2 

        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1 

        }
      } 
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
  
  $('.logo-slides').slick({
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
     autoplay: true,
    autoplaySpeed: 1000,
     responsive: [
       {
         breakpoint: 1221,
         settings: {
           slidesToShow: 5 
           
         }
       },
       {
         breakpoint: 768,
         settings: {
           slidesToShow: 3 
          
         }
       },
       {
         breakpoint: 480,
         settings: {
           slidesToShow: 1,
          
         }
       }
       // You can unslick at a given breakpoint now by adding:
       // settings: "unslick"
       // instead of a settings object
     ]
  });
  
  
   $('.province-logo-slides').slick({
    infinite: true,
    slidesToShow: 2.99,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
     autoplay: true,
    autoplaySpeed: 1000,
     responsive: [
       {
         breakpoint: 1221,
         settings: {
           slidesToShow: 3
           
         }
       },
       {
         breakpoint: 768,
         settings: {
           slidesToShow: 2
          
         }
       },
       {
         breakpoint: 480,
         settings: {
           slidesToShow: 1,
          
         }
       }
       // You can unslick at a given breakpoint now by adding:
       // settings: "unslick"
       // instead of a settings object
     ]
  });
  
  $('.feature-outer').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
     responsive: [
       {
         breakpoint: 1221,
         settings: {
           slidesToShow: 2 
           
         }
       },
       {
         breakpoint: 768,
         settings: {
           slidesToShow: 1 
          
         }
       } 
       // You can unslick at a given breakpoint now by adding:
       // settings: "unslick"
       // instead of a settings object
     ]
  });
   
  $('.slider-for').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    // fade: true,
    asNavFor: '.slider-nav',
    infinite: true,
    loop: true, 
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3 

        }
      },
      {
        breakpoint: 481,
        settings: {
          slidesToShow: 2 

        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
  $('.slider-nav').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    arrows: false,
     
    focusOnSelect: true
  });
  
  $('.slider-for-year').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav-year'
  });
  
  $('.slider-nav-year').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for-year',
    dots: false,
    arrows: false,
    centerMode: true,
    autoplay: true,
    autoplaySpeed: 3000,
    focusOnSelect: true,
    vertical: true,
    centerPadding: "0px",
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
           vertical: false,
          
        }
      } 
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
    
  });
  
  $(".slider-nav-year").on('wheel', (function(e) {
    e.preventDefault();

    if (e.originalEvent.deltaY < 0) {
      $(this).slick('slickNext');
    } else {
      $(this).slick('slickPrev');
    }
  })); 

  $('.vidPlay.youtube').on('click',function(){
    $('body').addClass('pOpen');
    $($(this).attr('href')).addClass('active');
    var getu = $('.videoPopup.active .tcInner:not(.o2)').find('iframe').attr('src');
    var getu2 = getu.replace("autoplay=0", "autoplay=1");
    $('.videoPopup.active .tcInner:not(.o2)').find('iframe').attr('src',getu2);
  });
  
  $('.tcClose.youtube').on('click',function(event){
    event.stopPropagation();
    var getu = $('.videoPopup.active .tcInner:not(.o2)').find('iframe').attr('src');
    var getu2 = getu.replace("autoplay=1", "autoplay=0");
    $('.videoPopup.active .tcInner:not(.o2)').find('iframe').attr('src',getu2);
    setTimeout (function(){
      $('.videoPopup.active .tcInner:not(.o2)').find('iframe').attr('src',getu);
    },500);
    $('body').removeClass('pOpen');
    $('.videoPopup').removeClass('active');
  });

  $('.vidPlay').on('click',function(){
    $('body').addClass('pOpen');
    $($(this).attr('href')).addClass('active');
  });

  $('.tcClose').on('click',function(event){
    event.stopPropagation();
    $('body').removeClass('pOpen');
    $('.videoPopup').removeClass('active');
  });
  
   $(".search-btn").on('click', function(){
    $(this).next().slideToggle(250);
   });
  
  $(".ui-group > span").on('click', function(){
    $(this).next().slideToggle(250);
  });
  
//   $('.pro-tab-sec .pro-tabs li:first-child').addClass('active');
//   $('.pro-tab-sec .pro-tab-desc').hide();
//   $('.pro-tab-sec .pro-tab-desc:first-child').show();
  
//   // Click function
//   $('.pro-tab-sec .pro-tabs li').click(function(){
//     $('.pro-tab-sec  .pro-tabs li').removeClass('active');
//     $(this).addClass('active');
//     $('.pro-tab-content .pro-tab-desc').hide();
     
//     var activeTab = $(this).find('a').attr('href');
//     $(activeTab).fadeIn();
//     return false;
//   });
  $('.pro-tabs ul li.tab-link:first-child').addClass('active');
   $('.pro-tab-content .pro-tab-desc.tab-content:first-child').addClass('active');
  $('.tab-link').click( function() {
	event.preventDefault(); 
	var tabID = $(this).attr('data-tab');
	
	$(this).addClass('active').siblings().removeClass('active');
	
	$('#tab-'+tabID).addClass('active').siblings().removeClass('active');
});
  
  // General Tab in Script
  $('.ui-tab li:first-child').addClass('active');
  $('.ui-content').hide();
  $('.ui-content:first-child').show();
  
  // Click function
  $('.ui-tab li').click(function(){
  
    $('.ui-tab li').removeClass('active');
    $('.ui-tab li a input').prop('checked', false);
    $(this).addClass('active');
   $(this).find('input[type="checkbox"]').prop('checked', true);
  
    $('.ui-content').hide();
    
    var uiTab = $(this).find('a').attr('href');
    $(uiTab).fadeIn();
   
    return false;
  });

$('.spec-tab li:first-child').addClass('active');
$('.spec-tab-content').hide();
$('.spec-tab-content:first').show();

// Click function
$('.spec-tab li').click(function(){
  $('.spec-tab li').removeClass('active');
  $(this).addClass('active');
  $('.spec-tab-content').hide();
  
  var activeTab = $(this).find('a').attr('href');
  $(activeTab).fadeIn();
  return false;
});
  $( "#checkbox-1" ).prop( "checked", true );
  
  
   
  
  $(".btn-form-link").on("click", function(){
   $($(this).attr("href")).addClass("form-active");
    $(".overlay_form_close").addClass("overlay-active"); 
   $("body").addClass("form-act"); 
  });
  
  $(".form-close").on("click", function(){
   $(".form-popup").removeClass("form-active");
    $("body").removeClass("form-act");
     $(".overlay_form_close").removeClass("overlay-active"); 
  });
  
   $(".overlay_form_close").on("click", function(){
     $(".overlay_form_close").removeClass("overlay-active"); 
    $(".form-popup").removeClass("form-active");
    $("body").removeClass("form-act");
  });
  
  
  
	
   $(window).load(function(){
     setTimeout(function(){
       $(".form-popup").removeClass("fm-act-not");
       $(".menu-popup").removeClass("menu-active-not");
       
     }, 500);
   });

  

  var seen = {};
  $('.tab-hubdb-section.o2 .pro-tabs li').each(function() {
    var txt = $(this).attr('data-value');
    if (seen[txt])
      $(this).remove();
    else
      seen[txt] = true;
  });
  
    $(".sidebar-tags li").slice(0, 11).show();
    $("body").on('click touchstart', '.see-more-btn', function (e) {
      e.preventDefault();
      $(".sidebar-tags li:hidden").slice(0, 11).slideDown();
      if ($(".sidebar-tags li:hidden").length == 0) {
        $(".see-more-btn").css('visibility', 'hidden');
      }
      $('html,body').animate({
        scrollTop: $(this).offset().top
      }, 1000);
    });
 
   if($(".similar-listing > article").length == 0){
     $(".similar-posts").addClass("sm-hide-sec");
    }
   else {
    $(".similar-posts").removeClass("sm-hide-sec");
   }
    
   
  $(".map-loc-box").each(function(){
   var mapDatasrc1 = $(this).attr('data-value'); 
      $('.map-block svg path[data-value="' + mapDatasrc1 + '"]').addClass("loc-avail");
  });
   
    $(".mapPart").each(function(){
      $(this).on('click', function(){
       $('.map-block .map-loc-box').removeClass('loc-active-pop');  
        $('.map-loc-box[data-value="' + $(this).attr("data-value") + '"]').toggleClass('loc-active-pop');
        return false;   
      });
    });
  
    $(document).on('click', 'body', function(e){
      $('.map-block .map-loc-box').removeClass('loc-active-pop'); 
     });
  
    $(document).on('click', '.map-loc-box', function(e){
      e.stopPropagation();
     });
  
   
   $(".dropdown-map a").on("click", function(){
      $('.map-loc-box').hide();
    $('.map-loc-box[data-value="' + $(this).attr("data-value") + '"]').fadeIn();
    $(".dropdown-inline > ul").slideUp(); 
    $(".dropdown-inline > span").text($(this).text()); 
   });
  
   $(".dropdown-inline > span").on("click", function(){
     $(this).next("ul").slideToggle(250);
   });
  
  if($('[data-fancybox]').length > 0) {
    $('[data-fancybox]').fancybox({
      youtube : {
        controls : 1,
        showinfo : 0
      },
      vimeo : {
        color : 'f00'
      }
    });
  }
  
  $('.video-wrapper:first-child').children().first().addClass('newClass');

  
  let slideLength = $('.hero-section .slick-track .slick-slide').length;
  if( slideLength == 1 ){
  $('.hero-section .slick-track .slick-slide .content-wrapper .slide-max').addClass('c-pad');
  }
  
   $(".prov-btn").on("click", function(){
      $(this).addClass('show');
   $("#locationModal").slideDown(500);
     $('body').addClass('fixed');
   });
   $(".locationclose, #close_and_clear_location").on("click", function(){
     
   $("#locationModal").slideUp(500);
      $('body').removeClass('fixed');
   });
  $(".map-overlay").on("click", function(){
     
   $("#locationModal").slideUp(500);
     $('body').removeClass('fixed');
   });
  
  $(".map-image svg a path, .locationmodal-footer a button.gen-btn, .country-dropdown select option").on("click", function(){
     var provinceCity = $(this).parent().attr("class");    
     if ($.cookie("name")) {
  
   $.removeCookie("name", { domain: 'essltd.com' }); // Cookie exists, delete it
   console.log("Existing cookie deleted.");
    }

    // Set the new cookie
    $.cookie("name", provinceCity, {
     expires: 60,
    domain: 'essltd.com',
    secure: true
    });
    console.log("New cookie set.");
    

  });   
   
 $(".tab-list-item p a.data-option").on("click", function(){
     var loginCity = $(this).parent().attr("data-login");  
      $.cookie("value", loginCity, { expires : 60 }); 
  });   
  
  
   $(".country-dropdown select").on("change", function(){
     var str = "";
     str = $('#province option:selected').attr("class");
     var provinceCity = str;    
    if ($.cookie("name")) {
  
   $.removeCookie("name", { domain: 'essltd.com' }); // Cookie exists, delete it
   console.log("Existing cookie deleted.");
    }

    // Set the new cookie
    $.cookie("name", provinceCity, {
     expires: 60,
    domain: 'essltd.com',
    secure: true
    });
    console.log("New cookie set.");
  }); 
  
   $(".tab-list-item p a.data-option, li.hs-menu-item a.data-option, .group-1 ul li.grp-filter a").on("click", function(){
     var filterOption = $(this).attr("data-option");
    $.cookie("type", filterOption, { expires : 60 });
  }); 
  
   $(" .group-2 ul li.grp-filter a").on("click", function(){
     var filterOption2 = $(this).attr("data-option");
    $.cookie("key1", filterOption2, { expires : 60 });   
     console.log("key 1 is"+filterOption2);
  }); 
  $(" .group-3 ul li.grp-filter a").on("click", function(){
     var filterOption3 = $(this).attr("data-option");
    $.cookie("key2", filterOption3, { expires : 60 });   
     console.log("key 2 is"+filterOption3);
  }); 
  $(" .group-4 ul li.grp-filter a").on("click", function(){
     var filterOption4 = $(this).attr("data-option");
    $.cookie("key3", filterOption4, { expires : 60 });   
     console.log("key 3 is"+filterOption4);
  }); 
  $(" .group-5 ul li.grp-filter a").on("click", function(){
     var filterOption5 = $(this).attr("data-option");
    $.cookie("key4", filterOption5, { expires : 60 });   
     console.log("key 4 is"+filterOption5);
  }); 
  
   $("li.login").on("click", function(){
    $(this).toggleClass('login-show');
  }); 
  
});
 

// $( window ).ready(function() { 
//   var cookieValue = $.cookie("name");
//     $('#province_text').text("You are browsing in " + cookieValue + "! " );

//     if (typeof $.cookie('name') === 'undefined'){
//     $("#locationModal").slideDown(500);
//     $('body').addClass('fixed');
      
// }  
// });



$( window ).ready(function() { 
  var currentURL = window.location.href;
  var cookieValue = $.cookie("name");
  if(typeof cookieValue != "undefined"){
    $('#province_text').text("You are browsing in " + cookieValue + "! " );
  }
  else{
    if (currentURL.startsWith("https://blog.essltd.com/ess-blog-2022")) {      
      $("#locationModal").slideUp(500);
      $('#province_text_undefined').css("display", "inline").removeClass('show_pv-text');
    }
    else{
      $("#locationModal").slideDown(500);
      $('body').addClass('fixed');
      $('#province_text_undefined').css("display", "inline").removeClass('show_pv-text');
    }  
  }
});




